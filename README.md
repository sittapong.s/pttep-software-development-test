# PTTEP Software Development Test

This is a project for software development testing.

1. There is only 1 project in the test. It is to build a single working app
2. Concentrate on the functionality of your app and closing the requirements. Visually attractive responsive design of the application is a nice to have
3. Do not share your code publicly for the test
4. You can use any Editors, or any development tools to finish your project
5. You must select database MySQL or MongoDB
6. Other tools - JDK x64 bit, Maven, NodeJS 

You also have a working internet connection to search for relevant code

You are free to use MySQL or MongoDB as your database. The data file to be used will be provided with the test
After Building your application, please do the following:
You need to send your source code and any configurations in the form of a combined zip file
Send screenshots of the application developed

Evaluation Criteria
Check functionality of the application
Check Source Code and Configuration files submitted
Quality of the Code
